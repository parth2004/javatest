import io.restassured.http.Method;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class RegisterSuccessful {
    @Test
    void main(){
        Response response = given().request(Method.POST,"https://reqres.in/api/register");
        System.out.println(response.getStatusCode());
    }
}
