import io.restassured.http.Method;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class SingleResourceNotFound {
    @Test
    void ResourecNotFound(){
        Response response = given().request(Method.GET,"https://reqres.in/api/unknown/23");
        System.out.println(response.getStatusCode());
    }
}
