import io.restassured.http.Method;
import io.restassured.response.Response;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

public class List {
    @Test
    void list(){
        Response response=given().request(Method.GET,"https://reqres.in/api/unknown");
        System.out.println(response.getStatusCode());
    }
}
