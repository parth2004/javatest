package testclasses;

import com.google.gson.JsonObject;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.response.Response;
import netscape.javascript.JSObject;
import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

   public class SingleUserNotFound {
            @Test
    public void getAllUsers(){
    RestAssured.baseURI = "https://reqres.in/api";
    Response response=given()
    .accept(ContentType.JSON)
    .request(Method.GET,"/users");
    System.out.println(response.asString());
    System.out.println(response.getStatusCode());
    JSONObject jsonObject = new JSONObject(response.asString());
    System.out.println(jsonObject.getInt("page"));
    System.out.println(jsonObject.getInt("per_page"));
    JSONArray jsonArray = jsonObject.getJSONArray("data");
    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
    System.out.println(jsonObject1.getString("email"));
    System.out.println(jsonObject1.getString("first_name"));
    System.out.println(jsonObject1.getInt("id"));
    }

    @Test
    public void getAllUser(){
    baseURI="https://reqres.in/api/users";
    Response response = given()
    .param("page", "2")

    .accept(ContentType.JSON)
    .request(Method.GET,baseURI);
    System.out.println(response.asString());
    System.out.println(response.getStatusCode());
    JSONObject jsonObject = new JSONObject(response.asString());
    System.out.println(jsonObject.getInt("page"));
    JSONArray jsonArray = jsonObject.getJSONArray("data");
    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
    System.out.println(jsonObject1.getString("email"));
    }

    @Test
    public void createUser(){
    baseURI = "https://reqres.in/api";
    String body = "{\"name\": \"morpheus\",\"job\": \"leader\"}";
    Response response = given()
    .body(body)
    .contentType(ContentType.JSON)
    .request(Method.POST,"/users");
     System.out.println(response.asString());
     System.out.println(response.getStatusCode());
     JSONObject jsonObject = new JSONObject(response.asString());
     System.out.println(jsonObject.getInt("id"));
      }

     @Test
     public void putUpdate(){
     baseURI = "https://reqres.in/api/users";
     String body = "{\n" +
     "    \"name\": \"Parth\",\n" +
      "    \"job\": \"zion resident\"\n" +
      "}";
       Response response = given()
      .contentType(ContentType.JSON)
      .body(body)
      .request(Method.PUT,"/2");
      System.out.println(response.asString());
      System.out.println(response.getStatusCode());
        }

       @Test
      public void deleteUser(){
      baseURI = "https://reqres.in/api/users";
      Response response = given()
      .request(Method.DELETE,"/2");
       System.out.println(response.asString());
       System.out.println(response.getStatusCode());
        }

        @Test
        public void getUserById(){
        baseURI = "https://reqres.in/api/users";
         Response response = given()
         .accept(ContentType.JSON)
         .request(Method.GET,"/2");
         System.out.println(response.asString());
         System.out.println(response.getStatusCode());
         JSONObject jsonObject = new JSONObject(response.asString());
         JSONObject jsonObject1 =  jsonObject.getJSONObject("data");
         }

        @Test
        public void registerSuccessful(){
        baseURI = "https://reqres.in/api";
        String body = "{\n" +
       "    \"email\": \"eve.holt@reqres.in\",\n" +
                        "    \"password\": \"pistol\"\n" +
                        "}";
        Response response = given()
        .accept(ContentType.JSON)
        .body(body)
        .request(Method.POST,"/register");
        System.out.println(response.asString());
         System.out.println(response.getStatusCode());
        }

        @Test
        public void DELETE(){
        baseURI = "https://reqres.in/api/users";
        Response response = given()
        .request(Method.DELETE,"/2");
        System.out.println(response.asString());
        System.out.println(response.getStatusCode());

        }
        }
