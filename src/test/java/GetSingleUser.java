import io.restassured.http.Method;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import java.net.URI;

import static io.restassured.RestAssured.given;

public class GetSingleUser {
    @Test
    void setup(){
        Response response= given().request(Method.GET,"https://reqres.in/api/users/2");
        System.out.println(response.getStatusCode());
    }

}
