import io.restassured.http.Method;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class Create {
    @Test
    void Practice(){
        Response response = given().request(Method.POST,"https://reqres.in/api/users");
        System.out.println(response.getStatusCode());
    }
}
