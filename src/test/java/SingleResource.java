import io.restassured.http.Method;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class SingleResource {
    @Test
    void main(){
        Response response = given().request(Method.GET,"https://reqres.in/api/unknown/2");
        System.out.println(response.getStatusCode());
    }

}
