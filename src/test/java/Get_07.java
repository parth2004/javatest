import io.restassured.http.Method;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class Get_07 {
    @Test
    void RequestSend(){
        Response response = given().request(Method.GET,"http://www.slf4j.org/codes.html#StaticLoggerBinder");
        System.out.println(response.getStatusCode());
    }
    @Test
    void Teardown(){
        System.out.println("Dlayed Request");
    }
}

