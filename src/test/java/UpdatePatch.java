import io.restassured.http.Method;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class UpdatePatch {
    @Test
    void test(){
        Response response = given().request(Method.PATCH,"https://reqres.in/api/users/2");
        System.out.println(response.getStatusCode());
    }
}
