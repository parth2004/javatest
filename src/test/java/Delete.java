import io.restassured.http.Method;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class Delete {
    @Test
    void main(){
        Response response = given().request(Method.DELETE,"https://reqres.in/api/users/2");
        System.out.println(response.getStatusCode());
    }
}
