import io.restassured.http.Method;
import io.restassured.response.Response;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;

public class Update {
    @Test
    void Main(){
        Response response = given().request(Method.PUT,"https://reqres.in/api/users/2");
        System.out.println(response.getStatusCode());
    }

}
